(function (require, module) {
   
   var Transform = require("stream").Transform
   var inherits = require("util").inherits

   // Constructor function
   function Streamify(options, transformFn, flushFn) {
      // Mapping arguments
      if (typeof options === "function") {
         flushFn = transformFn
         transformFn = options
         options = {objectMode: true}
      } else if (options.options) {
         flushFn = options.flush
         transformFn = options.transform
         options = options.options
      }
      
      Transform.call(this, options)
      this._transform = transformFn
      this._flush = flushFn
   }
   
   inherits(Streamify, Transform)
   
   module.exports = function (options, transformFn, flushFn) {
      return new Streamify(options, transformFn, flushFn)
   }

})(require, module)